import React,{useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';

const ArticleCard = (props) => {
    const {navigation,articleItem} = props;
    const LeftContent = props => <Avatar.Icon {...props} icon="folder" />
    const handle_navigateTo = () => {
        navigation.navigate('ArticleReading',{id:articleItem.id});
    }

    useEffect(() => {

    }, [])
    return (
        <Card style={{width:'48%',margin:6,borderRadius:12,overflow:'hidden'}}
            onPress={handle_navigateTo}
        >
                
                <Card.Cover source={{ uri: articleItem.image }} style={{height:120}} />
                <Text style={{fontSize:14,margin:6}}>{articleItem.name}</Text>
        </Card>
    )
}

export default ArticleCard

const styles = StyleSheet.create({})
