import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { SwipeListView } from 'react-native-swipe-list-view';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IconButton, Colors } from 'react-native-paper';
import {useDispatch} from 'react-redux';
import colors from '../../constants/Colors';

import * as wishlistActions from '../../store/actions/WhishlistActions';

const FavoriteItem = (props) => {
    const {data,navigation} = props;
    const dispatch = useDispatch();
    
    const handle_removeFavoriteVocabulary = (vocabulary) => {
        dispatch(wishlistActions.remove(vocabulary));
    }

    const handle_navigateToVocabulary = (vocabulary) => {
        navigation.navigate('Dictionary',{  screen: 'Search',params: {screen: 'Vocabulary',params: {id: vocabulary.id,},},})
    }
    return (
        <SwipeListView 
            data={data}
            renderHiddenItem={ (data, rowMap) => ( 
                <View  style={styles.swipeButtonContainer} >
                    <IconButton style={{alignSelf:'center'}}
                        icon="trash-can-outline"
                        color={colors.btn_danger}
                        size={33}
                        onPress={()=>handle_removeFavoriteVocabulary(data.item)}
                    />
                    <IconButton style={{alignSelf:'center'}}
                        icon="playlist-check"
                        color={colors.btn_primary}
                        size={33}
                        onPress={() => handle_navigateToVocabulary(data.item)}
                    />
                </View>
            )}
            keyExtractor={(item,index)=>String(index)}
            renderItem={ (data, rowMap) => (
            
                <View  style={styles.swipeItem} >
                    <Text style={styles.itemText}>{data.item.name}   <Text style={{fontSize:12}}>({data.item.type})</Text></Text>
                </View>
            )}
           
            rightOpenValue={-125}
            disableRightSwipe	
        />
    )
}

export default FavoriteItem

const styles = StyleSheet.create({
        itemText:{
            fontSize:16,
            fontWeight:'500',
            color:'white',
            height:'100%',
            paddingLeft:12,
            alignItems:'center',
            justifyContent:'center',
            alignContent:'center',
            paddingVertical:12
        },
        swipeButtonContainer:{
            backgroundColor:Colors.grey200,
            height:60,display:'flex',
            flexDirection:'row',
            justifyContent:'flex-end',
            marginVertical:2,
            alignItems:'center'
        },
        swipeItem:{
            height:60,
            backgroundColor:colors.bg_accent,
            marginVertical:1
        }
})
