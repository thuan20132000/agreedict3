import React,{useEffect, useState} from 'react'
import { View, Text,StyleSheet, Dimensions,TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/AntDesign";
import {useDispatch,useSelector } from 'react-redux';
import * as wishlistActions from '../store/actions/WhishlistActions';

export default function ListItem(props) {
    const {item,navigation,textStyle} = props;
    const dispatch = useDispatch();
    const [isExistInWishlist,setExistInWishlist] = useState(false);

    const {wishlist} = useSelector(state => state.wishlists);
    const checkExist = wishlist.findIndex(e=>e.id === item.id);
   
    const handleAddToWishlist = (item) => {
        setExistInWishlist(true);

        if(checkExist == -1){
            dispatch(wishlistActions.add(item));
        }
    }

    useEffect(() => {
        if(checkExist != -1){
            setExistInWishlist(true);
        }
    }, [])

    return (
        <View style={[styles.listItem,{backgroundColor:props.bgColor||'#E3EAF5'}]}>
            <TouchableOpacity style={[styles.vocabulary,{width:deviceWidth-60}]}
                onPress={()=>navigation.navigate('Dictionary',{  screen: 'Search',params: {screen: 'Vocabulary',params: {id: item.id,item:item},},})}
            >
                <Text style={textStyle}>{item.name} <Text style={{fontSize:14}}>({item.type})</Text> </Text>
                <Text>{item.pronunciation_us}</Text>  
            </TouchableOpacity>

            <TouchableOpacity style={styles.addWishlist}
                onPress={()=>handleAddToWishlist(item)}
            >
                <Icon style={styles.listIcon}
                    name={isExistInWishlist?'star':'staro'}
                    color={'#F5C81B'}
                    size={23} 
                />    
            </TouchableOpacity>
        </View>    
    )
}
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    listItem:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'flex-end',
        borderBottomWidth:2,
        borderBottomColor:'#EEF0F3',
        height:80

    },
    vocabulary:{
        padding:16
       
    },
    addWishlist:{
        display:'flex',
        justifyContent:'center',
        width:60,
        padding:16,
        height:'100%',
        backgroundColor:'#5C8DCA'
    },
    listVocabulary:{
        fontSize:22,
    }
})
