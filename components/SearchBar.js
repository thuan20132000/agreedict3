import React,{useState,useEffect,useRef} from 'react';
import { View, Text,StyleSheet,ScrollView, Dimensions } from 'react-native';
import ListItem from '../components/ListItem';
import { Searchbar,Colors,ActivityIndicator} from 'react-native-paper';

const SearchBar = (props) => {

    const {navigation} = props;
    const [search, setsearch] = useState('');
    const [searchTerm, setSearchTerm] = useState('');
    const typingTimeoutRef = useRef(null);
    const [errorNetwork, seterrorNetwork] = useState(false);
    const [listSearchAlpha, setListSearchAlpha] = useState(null);
    const [isLoadding,setIsLoadding] = useState(true);
    const [isSearching, setisSearching] = useState(false);

    
    useEffect(() => {
        
        seterrorNetwork(false);
        setsearch(searchTerm);
        return () =>{
            
        }
    },[searchTerm,search]);


    const handleSearchNewVocabulary = async () =>{
        try {
            setisSearching(true);
            const fetchSearch = await fetch(`http://207.148.110.78/search/${search}`);
            const resJson = await fetchSearch.json();
            let word_id = await resJson.word_definition.id;
            seterrorNetwork(false);
            navigation.navigate('Vocabulary',{id:word_id});

        } catch (error) {
            seterrorNetwork(true);
        }
        setisSearching(false);
    };



    const handleSearchAlphas = async (word) =>{
        
        try {
            setIsLoadding(true);
            const data = await fetch(`http://207.148.110.78/searchalphas/${word}`);
            const dataJson = await data.json();
            seterrorNetwork(false);
            setListSearchAlpha(dataJson.words_alphas);
            setIsLoadding(false);
            
        } catch (error) {
            seterrorNetwork(true);
            setIsLoadding(false);
        }  
    }

    const handleSearchTermChange = async  (text) => {
      
        const value = text.toLowerCase();
        setSearchTerm(value);
        if (typingTimeoutRef.current) {
            clearTimeout(typingTimeoutRef.current);
        }
        typingTimeoutRef.current = setTimeout(() => {
            if(text){
                handleSearchAlphas(text);
            }else{
                setListSearchAlpha(null);
            }
        },600);

    }

    const handleCancelSearch = () =>{
        setSearchTerm('');
    }


    return (
        <View>
            

            
            <Searchbar
                placeholder={'Enter to search'}
                onChangeText={(text)=>handleSearchTermChange(text)}
                value={searchTerm}
                returnKeyType={'search'}
                onSubmitEditing={handleSearchNewVocabulary}
            />

            {
                    isSearching && 
                    <View style={{marginVertical:30}}>
                        <ActivityIndicator style={{width:100,alignSelf:'center'}} size={'large'} color={Colors.redA200} />
                    </View>
                }
            {
                errorNetwork && 
                <View style={{padding:4}}>
                    <Text style={{fontFamily:'Roboto'}}>Sorry, this word cannot find. Try to enter a noun or verb of your word.</Text>
                </View>
            }
            
            {   
                (!isLoadding && listSearchAlpha) &&
                <ScrollView
                    contentInset={{
                        bottom:60,
                        top:0
                    }}
                >
                    {   
                        
                        Object.values(listSearchAlpha).map((item,index)=>
                        <ListItem key={item.id+index.toString()}
                            item={item}
                            navigation={props.navigation}
                            textStyle={styles.textStyle}
                            
                        />    
                        )
                    }
                </ScrollView>

            }
            
            
        </View>

    )
}
const Devwidth = Dimensions.get('screen').width;
const Devheight = Dimensions.get('window').height;

export default SearchBar

const styles = StyleSheet.create({
    searchBarContainer:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#435E7D',
        paddingVertical:26,
        paddingHorizontal:6
    },
    searchIcon:{
        position:'absolute',
        left:10
    },
    searchInput:{
        borderWidth:1,
        color:'white',
        borderColor:'#F8CB61',
        flex:1,
        padding:6,
        fontSize:22,
        borderRadius:26,
        paddingLeft:40,
        position:'relative',
    },
    searchButton:{
        width:100,
        height:80,
        display:'flex',
        alignContent:'center',
        justifyContent:'center'
    },
    searchButtonText:{
        fontSize:16,
        padding:12,
        textAlignVertical:'center',
        color:'white'
    },
    textStyle:{
        fontSize:22,
        color:Colors.black
    }

})