import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Icon from "react-native-vector-icons/AntDesign";
import { ScrollView } from 'react-native-gesture-handler';
import Sound from 'react-native-sound';
import {
    IconButton,
    Colors,
    Chip
} from 'react-native-paper';

export default function VocabularyBody(props) {
    const {wordDefinition,wordExplaination,id} = props;
    const handleSoundUs = async () =>{

        const sound =  new Sound(wordDefinition.sound_us, null, (error) => {
            if (error) {
                console.warn("errors in sound, Sorry!! We will fix this state in the near future.Thank");
            }
            // play when loaded
            sound.play();
        });

    }
    const handleSoundUk = async () =>{

        const sound =  new Sound(wordDefinition.sound_uk, null, (error) => {
            if (error) {
                console.warn("errors in sound, Sorry!! We will fix this state in the near future.Thank");
            }
            // play when loaded
            sound.play();
        });

    }


    
    return (
        <View style={styles.container}>
                            <ScrollView>

                <View style={styles.header}>
                    
                    <View style={{display:'flex',flexDirection:'row'}}>
                        <Text style={{color:'white',fontSize:26,fontFamily:'Roboto',fontWeight:'600'}}>{wordDefinition.name}</Text>
                        <Chip style={{backgroundColor:'#0DA6E4',marginHorizontal:10,justifyContent:'center'}}
                            onPress={() => console.log('Pressed')}
                        >
                            <Text style={{fontSize:14,fontWeight:'600',color:'white'}}>{wordDefinition.type}</Text>
                        </Chip>

                    </View>   

                    <View style={{display:'flex',flexDirection:'row'}}>
                        <View style={{display:'flex',justifyContent:'space-around'}}>
                            <Text style={styles.textPronunciation}><Text style={{color:'#DF330F',backgroundColor:'#F2C827'}}>us</Text>{wordDefinition.pronunciation_us}</Text>
                            <Text style={styles.textPronunciation}><Text style={{color:'white',backgroundColor:'#E16624'}}>uk</Text>{wordDefinition.pronunciation_uk}</Text>
                        </View>
                        <View>
                                <IconButton style={{backgroundColor:Colors.blue400}}
                                    icon="contactless-payment"
                                    color={Colors.white}
                                    size={26}
                                    onPress={handleSoundUs}
                                />
                                <IconButton style={{backgroundColor:Colors.blue400}}
                                    icon="contactless-payment"
                                    color={Colors.white}
                                    size={26}
                                    onPress={handleSoundUk}
                                />
                        </View>
                       
                    </View>

                </View>

                <View style={styles.body}>

                        {
                            wordExplaination.map((ex,index)=>
                                <View  style={{width:'100%',paddingBottom:12}} key={index}>
                                    <View style={styles.explaination}>
                                        {/* <Text style={{...styles.textExplainTitle},[{color:'white',fontSize:22,marginRight:6}]}>
                                           {index+1} 
                                        </Text> */}
                                        <Text style={styles.textExplainTitle}><Icon name={'star'} color='yellow' />{ex.title} </Text>
                                    </View>
                                    <View style={styles.explainExample}>
                                        {ex.example.map((e,index)=><Text key={index} style={styles.textExplainExample}> - {e}</Text>)}
                                    </View>
                                </View>

                            )
                        }
                    

                </View>
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
   
    header:{
        display:'flex',
        justifyContent:'space-between',
        flexDirection:'column',
        alignItems:'flex-start',
        margin:6,
        borderRadius:22,
        backgroundColor:'#435E7D',
        paddingVertical:6,
        paddingLeft:22,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.33,
        shadowRadius: 2.97,

        elevation: 21,
    },
    textName:{
        fontSize:28,
        color:'white',
        fontWeight:'500'
    },
    textPronunciation:{
        fontSize:16,
        color:'white'
    },
    body:{
        padding:4,
        margin:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.33,
        shadowRadius: 2.97,

        elevation: 21,    },
    textExplainTitle:{
        color:'white',
        fontSize:16,
        fontFamily:'Roboto',
        padding:8

    },
    textExplainExample:{
        color:'black',
        paddingLeft:20,
        fontSize:18,
        fontStyle:'italic',
        paddingVertical:4
    },
    explainExample:{
    },
    explaination:{
        // display:'flex',
        // flexDirection:'row',
        // alignItems:'flex-start',
        backgroundColor:'#536983',
        padding:6,
        borderRadius:8
    }
})
