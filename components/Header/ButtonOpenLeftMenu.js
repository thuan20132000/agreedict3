import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { IconButton, Colors } from 'react-native-paper';

const ButtonOpenLeftMenu = (props) => {
    const {navigation} = props;
    console.log(props);
    return (
        <IconButton
            icon="menu"
            color={Colors.white}
            size={36}
            onPress={() => navigation.toggleDrawer()}
        />
    )
}

export default ButtonOpenLeftMenu

const styles = StyleSheet.create({})
