/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React,{useEffect} from 'react';

import HomeScreen from './screens/HomeScreen';
import Router from './Router';


const App  = () => {
  
  return (
      <Router />
  );
};

export default App;
