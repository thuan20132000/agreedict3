/**
 * @format
 */
import * as React from 'react';

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { DefaultTheme,Provider as PaperProvider } from 'react-native-paper';
import { Provider as StoreProvider } from 'react-redux';
import { createStore,combineReducers,applyMiddleware } from 'redux';

import WhishlistReducer from './store/reducer/WhishlistReducer';
import RecentSearchReducer from './store/reducer/RecentSearchReducer';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import ReduxThunk from 'redux-thunk';


const rootReducer = combineReducers({
  wishlists: WhishlistReducer,
  recentSearchs:RecentSearchReducer

});

const store = createStore(rootReducer,applyMiddleware(ReduxThunk));


const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'tomato',
      accent: 'yellow',
    },
  };
  

export default function Main() {
   
    return (
    <StoreProvider store={store}>
        <PaperProvider theme={theme}
              settings={{
                    icon: props => <MaterialIcon {...props} />,
              }}          
        >
            <App />
        </PaperProvider>
    </StoreProvider>
    );
}
  
AppRegistry.registerComponent(appName, () => Main);
