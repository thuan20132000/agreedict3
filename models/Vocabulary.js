

class Vocabulary{
    constructor(id,name,sound_us,sound_uk,pronunciation_us,pronunciation_uk,type){
        this.id = id;
        this.name = name;
        this.sound_us = sound_us;
        this.sound_uk = sound_uk;
        this.pronunciation_us = pronunciation_us;
        this.pronunciation_uk = pronunciation_uk;
        this.type = type;
    }
}

export default Vocabulary;
