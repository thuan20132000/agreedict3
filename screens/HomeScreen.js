import React,{useEffect} from 'react'
import { View, Text, SafeAreaView, TextInput,StyleSheet } from 'react-native'
import SearchBar from '../components/SearchBar'
import { FAB, Portal, Provider } from 'react-native-paper';
import {useDispatch,useSelector } from 'react-redux';

import colors from '../constants/Colors';
import * as wishlistActions from '../store/actions/WhishlistActions';

export default function HomeScreen(props) {
    const dispatch = useDispatch();

    
    props.navigation.setOptions({
        title: 'Oxford Dictionary',
        headerStyle: {
          backgroundColor: colors.bg_primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },

    });

    useEffect(() => {
        dispatch(wishlistActions.set());
    }, [])

    return (
        <>
            <SafeAreaView>
            <SearchBar {...props} />
            
            </SafeAreaView>
            
        </>
    )
}
const styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
  })
  