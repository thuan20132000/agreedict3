import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { List } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';

const SpecialTopicScreen = (props) => {
    const {navigation} = props;
    return (
        <View>
            <ScrollView>
                <List.Item
                    title="Sport"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>navigation.navigate('SpecialTopicVocabulary')}
                />
                <List.Item
                    title="Hometown"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
                <List.Item
                    title="Food"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
                <List.Item
                    title="Life"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
                <List.Item
                    title="Education"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
                <List.Item
                    title="First Item"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
                <List.Item
                    title="First Item"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
                <List.Item
                    title="First Item"
                    description="Item description"
                    left={props => <List.Icon {...props} icon="folder" />}
                    onPress={()=>console.log('dsds')}
                />
            </ScrollView>

        </View>
    )
}

export default SpecialTopicScreen

const styles = StyleSheet.create({})
