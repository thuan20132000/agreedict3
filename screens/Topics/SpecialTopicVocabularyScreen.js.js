import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Chip } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';

const SpecialTopicVocabularyScreen = () => {
    return (
        <View>
            <Text>Topic Vocabulary</Text>
            <View style={styles.wordContainer}>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>abandon</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>abandon cdsds</Text>
                </TouchableOpacity><TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>abandon</Text>
                </TouchableOpacity><TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>abandon csdcds</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>alot</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>communication</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>abandon</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>About</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>abandon</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>expend</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>sound</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>share</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.wordItem}>
                    <Text style={styles.wordItemText}>characterictic</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default SpecialTopicVocabularyScreen

const styles = StyleSheet.create({
    wordContainer:{
        display:'flex',
        alignSelf:'center',
        flexDirection:'row',
        flexWrap:'wrap'
    },
    wordItem:{
        padding:16,
        backgroundColor:'coral',
        margin:4,
        borderRadius:6
    },
    wordItemText:{
        fontSize:18,
        color:'white'
    }
})
