import React,{useEffect, useState} from 'react'
import { StyleSheet, View } from 'react-native'
import {useSelector} from 'react-redux';
import colors from '../constants/Colors';
import { ActivityIndicator } from 'react-native-paper';
import FavoriteItem from '../components/WhishList/FavoriteItem';



export default function WhishlistScreen(props) {
    const {wishlist} = useSelector(state => state.wishlists); 
    const [isLoadding,setIsLoadding] = useState(true);
    
    const [localWishLists,setLocalWishLists] = useState();
    
    useEffect(() => {
        setLocalWishLists(wishlist);
        setIsLoadding(false);
    }, [wishlist]);
    
    props.navigation.setOptions({
        title: 'My Favorite Vocabulary',
        headerStyle: {
          backgroundColor: colors.bg_primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },

    });

    if(isLoadding){
        return <View><ActivityIndicator color={colors.primary} size={42} /></View>
    }

    return (
        <View >
             {
                 localWishLists && <FavoriteItem data={localWishLists} navigation={props.navigation} />
             }
        </View>
    )
}

const styles = StyleSheet.create({})
