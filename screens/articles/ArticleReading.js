import React, { useState,useEffect } from 'react'
import { StyleSheet, Text, View, Dimensions, SafeAreaView, Alert } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { ActivityIndicator,Card,IconButton, Title } from 'react-native-paper';

import {WebView} from 'react-native-webview';
import Colors from '../../constants/Colors';

const ArticleReading = (props) => {
    

    const {id} = props.route.params;
    const [article,setArticle] = useState();
    const [isloading,setIsLoading]= useState(true);
    const [content,setContent] = useState();
    const [articleTitle,setArticleTitle] = useState('');
    const [articleFontSize,setArticleFontSize] = useState(46);
    const [isLoaded,setIsLoaded] = useState(false);
    

    useEffect(() => {
        
        let isCancelled = false;
        const fetchArticle = async () => {
            setIsLoading(true);
            try {
                const res = await fetch(`https://276588c6820d.ngrok.io/api/post/${id}`);
                if(!res.ok){
                    console.warn('Please connect to internet')
                    return;
                }

                if(!isCancelled){
                    const data = await res.json();
                    setArticle(data);
                    setArticleTitle(data.name);
                }
                setIsLoading(false);   

            } catch (error) {
                if(!isCancelled){
                    Alert.alert("Error Connection");
                }
            }
            
        }

        fetchArticle();

        return () =>{
            isCancelled = true;
        }
        

    }, [id]);

    
    props.navigation.setOptions({
        title:'',
        headerRight:()=>(
            <View style={{display:'flex',flexDirection:'row'}}>
                <IconButton
                    icon="format-font-size-increase"
                    color={Colors.btn_danger}
                    size={28}
                    onPress={() => setArticleFontSize(articleFontSize+1)}
                />
                <IconButton
                    icon="format-font-size-decrease"
                    color={Colors.btn_danger}
                    size={28}
                    onPress={() => setArticleFontSize(articleFontSize-1)}
                />
            </View>
           
        ),
        headerBackTitle:''
           
        
        
    });

    const heightDev = Dimensions.get('window').height;

    if(isloading){
        return <ActivityIndicator size={33} color={'red'} />
    }

    return (
        <View style={{height:'100%',marginBottom:150}}>
            {/* <ScrollView
                directionalLockEnabled={true}
            > */}
                        {
                            !isloading &&
                            <>
                                <WebView  
                                contentInset={{bottom:120}}
                                style={{minHeight:heightDev,width:'100%'}}
                                originWhitelist={['*']}
                                source={{ html:'<div style="font-size:'+articleFontSize+'px;padding:0px 16px">'+article.content+'</div>' }}
                                startInLoadingState={true}
                                textZoom={100}
                                />
                            </>
                        }
                        
            {/* </ScrollView> */}
        </View>
    )
}

export default ArticleReading

const styles = StyleSheet.create({})
