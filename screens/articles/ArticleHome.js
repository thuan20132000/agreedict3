import React,{useEffect, useState} from 'react'
import { StyleSheet, Text, View, TouchableOpacity,Image,RefreshControl, Alert, ImageBackground } from 'react-native'
import ArticleCard from '../../components/Article/ArticleCard'
import { ScrollView, FlatList } from 'react-native-gesture-handler'
import { ActivityIndicator,Chip, Headline, Divider } from 'react-native-paper'
import Colors from '../../constants/Colors'
import { SafeAreaView } from 'react-native-safe-area-context'

const ArticleHome = (props) => {
    const {navigation} = props;

    const [isLoading,setIsLoading] = useState(true);
    const [articles,setArticles] = useState([]);
    const [articleTopic,setArticleTopic] = useState([]);
    const [postTopic,setPostTopic] = useState('');
    const [postNumber,setPostNumber] = useState(2);
    const [isLoadTopic,setIsLoadTopic] = useState(true);



    useEffect(() => {
        const fetchArticleTopic = async () => {
            setIsLoadTopic(true);
            try {
                const res = await fetch('https://276588c6820d.ngrok.io/api/topic');
              
                const data = await res.json();
                setArticleTopic(data.data);
                setIsLoadTopic(false);
            } catch (error) {
                Alert.alert('Error Connection!',"Please Connect to Internet."); 
            }
           
        }
        fetchArticleTopic();
        setPostTopic('');
    }, [])
    
    const [refreshing, setRefreshing] = useState(false);

    const fetchArticles = async () => {
        try {
            const res = await fetch(`http://276588c6820d.ngrok.io/api/post?post_num=${postNumber}&post_topic=${postTopic}`);
            if(!res.ok){
                return;
            }
            const data = await res.json();
            setArticles(data.data.data);
            setIsLoading(false);
        } catch (error) {
            Alert.alert('Error Connection!',"Please Connect to Internet."); 
        }
        

    }

    const handle_refreshArticle  =  () => {
        setRefreshing(true);
        setPostNumber(postNumber+2)
        fetchArticles();
        setRefreshing(false);
    }

    useEffect(() => {
        setIsLoading(true);
        setPostNumber(2);
        fetchArticles();
    }, [postTopic])

    
    return (
        <>  
            <View style={{margin:2}}>
                <Headline>Topics For You</Headline>
                <ScrollView horizontal={true} >
                    {
                        !isLoadTopic &&
                        
                            articleTopic.map((e,index)=>
                                <TouchableOpacity key={String(index)} 
                                    onPress={()=>setPostTopic(e.id)}
                                    style={[styles.topicItem,{width:160,height:100}]}
                                >
                                    <Text style={{fontSize:18,color:'white',fontWeight:'500'}}>{e.name}</Text>

                                    <ImageBackground
                                    source={{uri:e.image}} style={{width:100,height:100,marginHorizontal:20,marginTop:8}}
                                    >
                                    </ImageBackground>
                                </TouchableOpacity>
                            )
                        
                    }

                </ScrollView>
            </View>
            <Divider style={{marginVertical:16}}></Divider>
            
            {
                isLoading ?<ActivityIndicator style={[styles.indicatorLoad,{}]} color={Colors.btn_danger} size={34} />
                :

                        <FlatList
                            data={articles}
                            renderItem={({ item, index, separators })=><ArticleCard key={String(index)} articleItem={item} navigation={navigation} />}
                            keyExtractor={(item,index) => String(index)}
                            numColumns={2}
                            columnWrapperStyle={{justifyContent:'space-around'}}
                        
                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={handle_refreshArticle} />
                            }
                        
                        />

            }
          
        </>
    )
}

export default ArticleHome

const styles = StyleSheet.create({
    container:{
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap',
    },
    topicItem:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1.84,
        backgroundColor:'dodgerblue',
        
        elevation: 5,
        marginHorizontal:6,
        paddingVertical:14,
        marginVertical:6,
        padding:4,
        borderRadius:10

    },
    indicatorLoad:{
        alignSelf:'center',
        display:'flex',
        justifyContent:'center',
        alignItems:'center'
    }
    
})
