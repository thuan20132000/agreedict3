import React,{useEffect, useState} from 'react'
import { StyleSheet, View,Text } from 'react-native'
import {useSelector} from 'react-redux';
import colors from '../constants/Colors';
import { ActivityIndicator,Chip } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';



export default function RecentSearchScreen(props) {
    const {recentSearch} = useSelector(state => state.recentSearchs); 
    const [isLoadding,setIsLoadding] = useState(true);
    const [recentSearchData,setRecentSearchData] = useState();
    
    useEffect(() => {
        setRecentSearchData(recentSearch);
        setIsLoadding(false);
    }, [recentSearch]);
    
    props.navigation.setOptions({
        title: 'Recent Search Vocabulary',
        headerStyle: {
          backgroundColor: colors.bg_primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },

    });


    if(isLoadding){
        return <View><ActivityIndicator color={colors.btn_danger} size={42} /></View>
    }

    return (
        <ScrollView>
            <View style={styles.recentSearchContainer}>


                {
                    recentSearchData.map((e,index)=>(
                        <Chip key={String(index)}
                            onPress={() => console.log('Pressed')}
                            style={styles.recentSearchItem}
                            textStyle={styles.recentSearchItemText}
                        >
                            {e.name} <Text style={{fontSize:12,fontWeight:'200'}}>({e.type})</Text>
                        </Chip>
                    ))
                }
               
              
            </View>
        </ScrollView>

    )
}

const styles = StyleSheet.create({
        recentSearchContainer:{
            display:'flex',
            flexDirection:'row',
            flexWrap:'wrap',
            paddingHorizontal:8,
            paddingTop:12,
            justifyContent:'flex-start'
        },
        recentSearchItem:{
            padding:6,
            marginHorizontal:8,
            marginVertical:2,
            backgroundColor:colors.bg_menu_item
        },
        recentSearchItemText:{
            fontSize:16,
            fontWeight:'500',
            color:'white'
        }
})
