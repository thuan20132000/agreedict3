import React,{useEffect,useState} from 'react'
import { View,StyleSheet, Alert } from 'react-native'
import VocabularyBody from '../components/VocabularyBody';
import colors from '../constants/Colors';
import { IconButton, ActivityIndicator,Colors } from 'react-native-paper';
import {useDispatch,useSelector } from 'react-redux';
import * as wishlistActions from '../store/actions/WhishlistActions';
import * as recentSearchActions from '../store/actions/RecentSearchActions';

const VocabularyScreen = (props) => {
    const {id,item}  = props.route.params;
    const dispatch = useDispatch();
    const [wordDefinition,setWordDefinition] = useState();
    const [wordExplaination,setWordExplaination] = useState();
    const [isLoading,setIsLoading] = useState(true);
    const [isExist,setExist] = useState(false);

    const {wishlist} = useSelector(state => state.wishlists);
    
    const handleAddToWishlist = () => {
        if(!isExist){
            dispatch(wishlistActions.add(item));
            setExist(true);
        }
    }


    useEffect(() => {
        const handleGetVocabulary = async () =>{
            setIsLoading(true);

            try {
                const res = await fetch(`http://207.148.110.78/vocabulary/${id}`);
                const resJson = await res.json();
                if(resJson.state !== "error"){
                    setWordDefinition(resJson.word_definition);
                    setWordExplaination(resJson.word_explaination);
                    dispatch(recentSearchActions.add(resJson.word_definition));
                    const checkExist = await wishlist.findIndex(e=>e.name === resJson.word_definition.name);
                    if(checkExist == -1){
                        setExist(false);
                    }else{
                        setExist(true);
                    }
    
                }
                setIsLoading(false);
            } catch (error) {
                Alert.alert('Connection Error!',"Please check your connection.")
            }
           


        }
        handleGetVocabulary();
        

    },[id]);

    props.navigation.setOptions({
        title: 'Oxford Dictionary',
        headerStyle: {
          backgroundColor: colors.bg_primary,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        headerRight: () => (
                    <IconButton  style={{marginHorizontal:12}}
                        size={34} 
                        icon={isExist?'star':'star-outline'}  
                        color={'#F5C81B'} 
                        onPress={()=>handleAddToWishlist()}
                    />
        ) 

    })
    return (
        <View>
            {
                !isLoading ?
                <VocabularyBody 
                    id={id}
                    wordDefinition={wordDefinition} 
                    wordExplaination={wordExplaination} 
                />:
                <ActivityIndicator color={Colors.primary} size={42} style={{}}/>
                
            }
            
        </View>

    )
}
export default VocabularyScreen

const styles = StyleSheet.create({
    headerRightButton:{
        marginRight:22,
        borderWidth:1,
        paddingLeft:6,
        paddingRight:6,
        borderRadius:16,
        borderColor:'red'
    }
});