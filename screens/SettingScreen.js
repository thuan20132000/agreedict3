import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Switch } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';


const SettingScreen = () => {
    const [isSwitchOn_offlineContent, setIsSwitchOn_offlineContent] = React.useState(false);
    const [isSwitchOn_wordNotification, setIsSwitchOn_wordNotification] = React.useState(false);
    const [isSwitchOn_advertisement, setIsSwitchOn_advertisement] = React.useState(false);

    const onToggleSwitch_offlineContent = () => setIsSwitchOn_offlineContent(!isSwitchOn_offlineContent);
    const onToggleSwitch_wordNotification = () => setIsSwitchOn_wordNotification(!isSwitchOn_wordNotification);
    const onToggleSwitch_advertisement = () => setIsSwitchOn_advertisement(!isSwitchOn_advertisement);

  
  
    return (
        <SafeAreaView>
            <View style={styles.settingContainer} >
                <View style={styles.settingItem}>
                    <Text style={styles.itemText}>Enable Offline Content</Text>
                    <Switch value={isSwitchOn_offlineContent} onValueChange={onToggleSwitch_offlineContent} color={'#F4461C'} />
                </View>
                <View style={styles.settingItem}>
                    <Text style={styles.itemText}>Word of the day notification</Text>
                    <Switch value={isSwitchOn_wordNotification} onValueChange={onToggleSwitch_wordNotification} color={'#F4461C'} />
                </View>
                <View style={styles.settingItem}>
                    <Text style={styles.itemText}>Disable Advertisements</Text>
                    <Switch value={isSwitchOn_advertisement} onValueChange={onToggleSwitch_advertisement} color={'#F4461C'} />
                </View>

               
               
            </View>
        </SafeAreaView>
        
    )
}

export default SettingScreen

const styles = StyleSheet.create({
    settingContainer:{
        display:'flex',
        flexDirection:'column'
    },
    settingItem:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'#1C91C1',
        padding:12,
        marginVertical:6,
        paddingHorizontal:22
    },
    itemText:{
        fontSize:18,
        fontWeight:'400',
        color:'white'
    },
    settingThemeColor:{
        display:'flex',
        marginTop:32

    },
    themeItem:{
        padding:22,
    }

})
