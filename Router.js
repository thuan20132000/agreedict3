
import * as React from 'react';
import {View,Text,StyleSheet, SafeAreaView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { 
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';

import Icon from "react-native-vector-icons/AntDesign";
import { Avatar ,Drawer,List} from 'react-native-paper';
import colors from './constants/Colors';


import HomeScreen from './screens/HomeScreen';
import VocabularySceen from './screens/VocabularyScreen';
import WhishlistScreen from './screens/WhishlistScreen';
import SettingScreen from './screens/SettingScreen';
import SpecialTopicScreen from './screens/Topics/SpecialTopicScreen';
import SpecialTopicVocabularyScreen from './screens/Topics/SpecialTopicVocabularyScreen.js';
import ArticleHome from './screens/articles/ArticleHome';
import ArticleReading from './screens/articles/ArticleReading';
import ButtonOpenLeftMenu from './components/Header/ButtonOpenLeftMenu';
import RecentSearchScreen from './screens/RecentSearchScreen';
import WordOfTheDayScreen from './screens/WordOfTheDayScreen';

const Stack = createStackNavigator();
const SearchStack = () =>{
    return (
        <Stack.Navigator
            screenOptions={({navigation})=> ({
                        headerLeft: () => (
                          <ButtonOpenLeftMenu navigation={navigation} />
                        ),
            })}
        >
                <Stack.Screen
                    name="Home" 
                    component={HomeScreen}
                  
                     
                />
                <Stack.Screen 
                    name="Vocabulary" 
                    component={VocabularySceen}
                  
                />
        </Stack.Navigator>
    )
}

const ArticleStack = () =>{
    return (
        <Stack.Navigator>
            <Stack.Screen name="ArticleHome" component={ArticleHome} />
            <Stack.Screen name="ArticleReading" component={ArticleReading} />

        </Stack.Navigator>
    )
}

const SettingStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Settings" component={SettingScreen} />
        </Stack.Navigator>
    )
}

const WishListStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Favorites" component={WhishlistScreen} />
        </Stack.Navigator>
    )
}


const RecentSearchStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Recent" component={RecentSearchScreen} />
        </Stack.Navigator>
    )
}


const WordOfDayStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="WordOfDay" component={WordOfTheDayScreen} />
        </Stack.Navigator>
    )
}

const SpecialTopicStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name="SpecialTopicList"
                component={SpecialTopicScreen} 
            
            />
            <Stack.Screen 
                name="SpecialTopicVocabulary" 
                component={SpecialTopicVocabularyScreen} 
            />

        </Stack.Navigator>
    )
}



const Tab = createBottomTabNavigator();
const SearchTab = () =>{
    return (
            <Tab.Navigator 
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                      let iconName;
          
                      if (route.name === 'Search') {
                            iconName = "search1";
                            color = focused?'salmon':'white'

                      } else if (route.name === 'Article') {
                            iconName ='profile';
                            color = focused?'salmon':'white'
                      }
                      // You can return any component that you like here!
                      return <Icon name={iconName} color={color} size={28} />;
                    },

                  })}
                  tabBarOptions={{
                    activeTintColor:'white',
                    inactiveTintColor: 'gray',
                    safeAreaInsets:'bottom',
                    tabStyle:{
                        backgroundColor:colors.bg_primary,
                      
                    },
                    showLabel:false,
                    safeAreaInsets:{
                        bottom:0
                    }

                  }}
            >
                
                <Tab.Screen name="Search" component={SearchStack} />
                <Tab.Screen name="Article" component={ArticleStack} />
            </Tab.Navigator>

    )
}





function CustomDrawerContent(props) {
    const {navigation} = props;
    const [active, setActive] = React.useState('dictionary');

    const handle_navigateTo = (val,active) => {
        setActive(active);
        props.navigation.navigate(val);

    }

    return (
        <DrawerContentScrollView {...props} >
            <View style={{alignSelf:'center',marginBottom:16}}>
                <Avatar.Image size={164} source={require('./assets/images/logo.png')} />
            </View>
            <View>
          
            <List.Item
                title="Dictionary"
                left={props => <List.Icon {...props} icon="book-search" color={active==='dictionary'?'white':'grey'}  />}
                titleStyle={{fontSize:18,fontWeight:'500',color:active==='dictionary'?'white':'black'}}
                onPress={()=>handle_navigateTo('Dictionary','dictionary')}
                style={{backgroundColor:active==='dictionary'?'#1977C8':'white'}}

            />
        
            <List.Item
                title="Favorites"
                left={props => <List.Icon {...props} icon="star-outline" color={active==='favorites'?'white':'grey'} />}
                style={{backgroundColor:active==='favorites'?'#1977C8':'white'}}
                titleStyle={{fontSize:18,fontWeight:'500',color:active==='favorites'?'white':'black'}}
                onPress={()=>handle_navigateTo('Favorites','favorites')}
            />
            <List.Item
                title="Recent"
                left={props => <List.Icon {...props} icon="clock-fast" color={active==='recent'?'white':'grey'} />}
                style={{backgroundColor:active==='recent'?'#1977C8':'white'}}
                titleStyle={{fontSize:18,fontWeight:'500',color:active==='recent'?'white':'black'}}
                onPress={()=>handle_navigateTo('Recent','recent')}

            />
            <List.Item
                title="Settings"
                left={props => <List.Icon {...props} icon="cog-outline" color={active==='settings'?'white':'grey'} />}
                style={{backgroundColor:active==='settings'?'#1977C8':'white'}}
                titleStyle={{fontSize:18,fontWeight:'500',color:active==='settings'?'white':'black'}}
                onPress={()=>handle_navigateTo('Settings','settings')}
            />

            </View>
        </DrawerContentScrollView>


    );
}

const DrawerSearch = createDrawerNavigator();
const SearchDrawer = (props) => {
    return (
            <DrawerSearch.Navigator initialRouteName="Dictionary"
                drawerContent={(props) => <CustomDrawerContent {...props}/> }
            >
                <DrawerSearch.Screen name="Dictionary" component={SearchTab} />
                <DrawerSearch.Screen name="Settings" component={SettingStack} />
                <DrawerSearch.Screen name="WordOfDay" component={WordOfDayStack} />
                <DrawerSearch.Screen name="Favorites" component={WishListStack} />
                <DrawerSearch.Screen name="Recent" component={RecentSearchStack} />

            </DrawerSearch.Navigator>
    )
}


const Router = () => {
    return (
        <NavigationContainer>
            <SearchDrawer/>
        </NavigationContainer>  
    ) 
}


export default Router

const styles = StyleSheet.create({})
