import Vocabulary from '../../models/Vocabulary';
import {ADD} from '../actions/WhishlistActions';
import AsyncStorage from '@react-native-community/async-storage';

const initialState= {
    wishlist :[],
    amount:0
}


export default (state = initialState,action) =>{


    switch (action.type) {

        case 'SET':
            var localValueArray = action.localVocabulary;
            return {...state,wishlist:localValueArray}

        case 'ADD':

            var localValueArray = action.localVocabulary;
            var newVocabularyObject = action.newVocabulary;
            
            if(action.newVocabulary){
              
                localValueArray.push(newVocabularyObject);

                storeData(localValueArray);
                
                return {...state,wishlist:localValueArray}
            }else{
                return {...state}
            }
           
        

        case 'REMOVE':
            var newWishlist = state.wishlist;
            if(action.removeVocabulary){
                var index = newWishlist.findIndex(e=>e.id ==action.removeVocabulary.id);
                newWishlist.splice(index,1);
                storeData(newWishlist);
            }
            
            return {...state,wishlist:newWishlist}

        default:
            break;
    }


    return state;
}

const storeData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem('@vocabulary_wishlist', jsonValue)
    } catch (e) {
      // saving error
    }
  }
  