import Vocabulary from '../../models/Vocabulary';
import {ADD_RECENT_SEARCH} from '../actions/RecentSearchActions';

const initialState= {
    recentSearch :[],
    amount:0
}


export default (state = initialState,action) =>{


    switch (action.type) {

        case 'ADD_RECENT_SEARCH':
            console.log('reducer recent search');
            var newRecentSearch = state.recentSearch;
            var newVocabularyObject = action.newVocabulary;
            
            if(action.newVocabulary){
              
                newRecentSearch.push(newVocabularyObject);

                return {...state,recentSearch:newRecentSearch}
            }else{
                return {...state}
            }
           
        
        default:
            break;
    }


    return state;
}
