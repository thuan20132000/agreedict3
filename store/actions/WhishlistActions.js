import AsyncStorage from '@react-native-community/async-storage';

export const ADD = 'ADD';
export const SET = 'SET';
export const REMOVE = 'REMOVE';
export const UPDATE = 'UPDATE';




export function set(){
    
    return async (dispatch) => {
            let localVocabulary = [];
            try {
            const jsonValue = await AsyncStorage.getItem('@vocabulary_wishlist');
            if(jsonValue != null){
                 localVocabulary = await   JSON.parse(jsonValue);
            }
            //   return jsonValue != null ? JSON.parse(jsonValue) : null;
            } catch(e) {
            console.log('error: ',e);
         
            }


        dispatch({
            type:SET,
            localVocabulary:localVocabulary
        })
    }

}


export function add(newVocabulary){
    
        return async (dispatch) => {
                let localVocabulary = [];
                try {
                const jsonValue = await AsyncStorage.getItem('@vocabulary_wishlist');
                if(jsonValue != null){
                    localVocabulary =  await JSON.parse(jsonValue);
                }
                //   return jsonValue != null ? JSON.parse(jsonValue) : null;
                } catch(e) {
                // error reading value
                console.log('error: ',e);
                }

            dispatch({
                type:ADD,
                newVocabulary:newVocabulary,
                localVocabulary:localVocabulary
            })
        }

}


export function remove(vocabulary){
    
    return async (dispatch) => {

    dispatch({
        type:REMOVE,
        removeVocabulary:vocabulary,
    })
}

}